# Planner

Módulo que realiza la planificación de la planta eólica.

## Detalles de implementación.

* Se implementará en Java con SpringBoot
* Cuando tenga que realizar una nueva planta eólica llamará a los servicios
`WeatherService` y `TopoService` en paralelo. Para ello se usará la [capacidad de Spring][1] de ejecutar tareas en
 segundo plano (asíncronas) sin gestionar de 
forma explícita los Threads.
* Cuando reciba la respuesta de cada uno de los servicios enviará un mensaje a la cola de RabbitMQ para que el server
 pueda notificar al browser y actualizar su estado en la base de datos.
* Se simulará un tiempo de proceso de 1 a 3 segundos aleatorio.
* El resultado de la creación de la planta será creará de la siguiente forma: la
ciudad concatenada a la respuesta del servicio que responda primero concatenada a la respuesta del segundo. El resultado se convertirá a lowercase si la ciudad empieza por una letra igual o anterior a M o en uppercase si es posterior.
* El progreso se calculará de la siguiente forma:
    * 25% cuando las peticiones a los servicios se hayan enviado
    * 50% cuando llegue la respuesta al primer servicio
    * 75% cuando llegue la respuesta al segundo
    * 100% cuando se haya creado la planificación (concatenando las cadenas)
    
    
[1]: https://spring.io/guides/gs/async-method/