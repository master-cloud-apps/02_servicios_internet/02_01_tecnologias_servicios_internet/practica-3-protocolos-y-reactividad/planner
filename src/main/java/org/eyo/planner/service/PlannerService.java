package org.eyo.planner.service;

import org.eyo.planner.model.Plan;
import org.eyo.planner.model.PlanRequest;

import java.util.concurrent.CompletableFuture;

public interface PlannerService {
    CompletableFuture<Plan> getPlanFromCity(PlanRequest cityPlan);
}
