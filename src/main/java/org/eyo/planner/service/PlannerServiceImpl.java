package org.eyo.planner.service;

import org.eyo.planner.model.Plan;
import org.eyo.planner.model.PlanRequest;
import org.eyo.planner.repository.TopoService;
import org.eyo.planner.repository.WeatherRepository;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

@Service
public class PlannerServiceImpl implements PlannerService {

    private TopoService topoService;
    private WeatherRepository weatherService;
    private RabbitTemplate rabbitTemplate;

    @Value("${eoloplant.createPlantProgress.queue}")
    private String plantProgressQueueName;

    public PlannerServiceImpl(TopoService topoService, WeatherRepository weatherService,
                              RabbitTemplate rabbitTemplate) {
        this.topoService = topoService;
        this.weatherService = weatherService;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public CompletableFuture<Plan> getPlanFromCity(PlanRequest cityPlanRequest) {
        Plan cityPlan = this.createPlan(cityPlanRequest);
        this.sendCityPlanToQueue(cityPlan);

        CompletableFuture<Void> topo = this.topoService.getTopoFromCityName(cityPlanRequest.getCity())
                .thenAccept(topoDetail -> {
                    modifyPlan(cityPlan, topoDetail.getLandscape());
                    this.sendCityPlanToQueue(cityPlan);
                });
        CompletableFuture<Void> weather = this.weatherService.getCityWeather(cityPlanRequest.getCity())
                .thenAccept(weatherResponse -> {
                    modifyPlan(cityPlan, weatherResponse.getWeather());
                    this.sendCityPlanToQueue(cityPlan);
                });


        CompletableFuture.allOf(topo, weather).join();
        Executor delayed = CompletableFuture.delayedExecutor(new Random().nextInt(3) + 1L, TimeUnit.SECONDS);
        return CompletableFuture
                .supplyAsync(() -> {
                    completePlantAndSendToQueue(cityPlan);
                    return cityPlan;
                }, delayed);
    }

    private Plan createPlan(PlanRequest cityPlanRequest) {
        Plan cityPlan = new Plan();
        cityPlan.setPlanning(cityPlanRequest.getCity());
        cityPlan.setId(cityPlanRequest.getId());
        cityPlan.setCity(cityPlanRequest.getCity());
        cityPlan.setCompleted(false);
        cityPlan.setProgress(25);
        return cityPlan;
    }

    private void modifyPlan(Plan planToModify, String increasedPlanningString) {
        planToModify.setPlanning(planToModify.getPlanning() + "-" + increasedPlanningString);
        planToModify.setProgress(planToModify.getProgress() + 25);

    }

    private void sendCityPlanToQueue(Plan cityPlan) {
        rabbitTemplate.convertAndSend(plantProgressQueueName, cityPlan);
    }

    private void completePlantAndSendToQueue(Plan cityPlan) {
        cityPlan.setProgress(cityPlan.getProgress() + 25);
        cityPlan.setCompleted(true);
        if (cityPlan.getPlanning().charAt(0) <= 'M'){
            cityPlan.setPlanning(cityPlan.getPlanning().toLowerCase());
        } else{
            cityPlan.setPlanning(cityPlan.getPlanning().toUpperCase());
        }
        this.sendCityPlanToQueue(cityPlan);
    }
}
