package org.eyo.planner.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Plan {

    private int id;
    private String city;
    private int progress;
    private Boolean completed;
    private String planning;
}
