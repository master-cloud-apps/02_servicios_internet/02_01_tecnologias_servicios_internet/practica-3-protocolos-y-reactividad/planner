package org.eyo.planner.repository;

import org.eyo.grpc.GetWeatherRequest;
import org.eyo.grpc.Weather;
import org.eyo.grpc.WeatherServiceGrpc;
import org.springframework.stereotype.Repository;

import java.util.concurrent.CompletableFuture;

@Repository
public class WeatherRepository implements WeatherGRPC {

    private WeatherServiceGrpc.WeatherServiceBlockingStub client;

    public WeatherRepository(WeatherServiceGrpc.WeatherServiceBlockingStub client) {
        this.client = client;
    }

    @Override
    public CompletableFuture<Weather> getCityWeather(String cityName) {
        return CompletableFuture
                .supplyAsync(() -> client.getWeather(buildRequest(cityName)))
                .thenApply(weather -> weather);
    }

    private GetWeatherRequest buildRequest(String cityName){
        return GetWeatherRequest.newBuilder().setCity(cityName).build();
    }
}
