package org.eyo.planner.repository;

import org.eyo.planner.model.TopographicDetailsDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Repository
public class TopoRepository implements TopoService {

    private final RestTemplate restTemplate;

    @Value("${topoService.url}")
    private String topoServiceUri;

    public TopoRepository(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public CompletableFuture<TopographicDetailsDTO> getTopoFromCityName(String cityName) {
        return CompletableFuture
                .supplyAsync(() -> restTemplate.getForObject(getTopoURI(cityName), TopographicDetailsDTO.class))
                .thenApply(topoCity -> {
                    if (topoCity == null) {
                        return new TopographicDetailsDTO(cityName, "flat");
                    }
                    return topoCity;
                });
    }

    private URI getTopoURI(String cityName) {
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("city", cityName);
        return UriComponentsBuilder.fromUriString(topoServiceUri).buildAndExpand(urlParams).toUri();
    }
}
