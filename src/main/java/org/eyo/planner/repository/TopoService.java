package org.eyo.planner.repository;

import org.eyo.planner.model.TopographicDetailsDTO;
import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.CompletableFuture;

public interface TopoService {
    @Async
    CompletableFuture<TopographicDetailsDTO> getTopoFromCityName(String cityName);
}
