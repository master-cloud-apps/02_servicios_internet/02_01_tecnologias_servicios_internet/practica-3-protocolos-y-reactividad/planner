package org.eyo.planner.repository;

import org.eyo.grpc.Weather;

import java.util.concurrent.CompletableFuture;

public interface WeatherGRPC {
    CompletableFuture<Weather> getCityWeather(String cityName);
}
