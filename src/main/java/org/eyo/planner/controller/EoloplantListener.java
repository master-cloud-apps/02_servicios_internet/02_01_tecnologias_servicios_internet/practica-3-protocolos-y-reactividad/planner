package org.eyo.planner.controller;

import org.eyo.planner.model.Plan;
import org.eyo.planner.model.PlanRequest;
import org.eyo.planner.service.PlannerService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;

@Component
public class EoloplantListener {

    private PlannerService plannerService;

    public EoloplantListener(PlannerService plannerService) {
        this.plannerService = plannerService;
    }

    @RabbitListener(queues = "${eoloplant.createPlant.queue}", ackMode = "AUTO")
    @SendTo("status")
    public Plan received(PlanRequest cityPlan) throws ExecutionException, InterruptedException {
        return this.plannerService.getPlanFromCity(cityPlan).get();
    }
}
