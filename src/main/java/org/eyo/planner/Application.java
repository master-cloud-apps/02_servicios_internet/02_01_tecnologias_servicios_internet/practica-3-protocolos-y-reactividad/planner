package org.eyo.planner;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.eyo.grpc.WeatherServiceGrpc;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAsync
public class Application {

    @Value("${weatherServiceJS.server}")
    private String weatherServer;
    @Value("${weatherServiceJS.port}")
    private int weatherPort;
    @Value("${eoloplant.createPlant.queue}")
    private String createPlantQueueName;
    @Value("${eoloplant.createPlantProgress.queue}")
    private String plantProgressQueueName;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public WeatherServiceGrpc.WeatherServiceBlockingStub client() {
        ManagedChannel channel =
                ManagedChannelBuilder.forAddress(weatherServer, weatherPort)
                        .usePlaintext()
                        .build();
        return WeatherServiceGrpc.newBlockingStub(channel);
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public Queue createPlantQueue() {
        return new Queue(createPlantQueueName, false);
    }

    @Bean
    public Queue plantProgressQueue() {
        return new Queue(plantProgressQueueName, false);
    }


}
