package org.eyo.planner;

import org.eyo.planner.controller.EoloplantListener;
import org.eyo.planner.model.Plan;
import org.eyo.planner.model.PlanRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EoloplantListenerTest {

    private EoloplantListener eoloplantListener;

    @BeforeEach
    void setUp() {
        this.eoloplantListener = new EoloplantListener(cityPlan -> CompletableFuture.completedFuture(new Plan(1,
                "Pontevedra", 100,
                true,
                "Pontevedra" +
                        "-Sunny-Flat")));
    }

    @Test
    void whenReceive_shouldCallService() throws ExecutionException, InterruptedException {
        PlanRequest request = new PlanRequest(1, "Pontevedra");
        Plan testPlan = this.eoloplantListener.received(request);

        assertEquals(100, testPlan.getProgress());
    }
}
