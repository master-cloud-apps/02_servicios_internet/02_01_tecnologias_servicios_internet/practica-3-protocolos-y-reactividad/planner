package org.eyo.planner;

import org.eyo.planner.model.TopographicDetailsDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class TopographicDetailsDTOTest {

    private TopographicDetailsDTO topo;

    @BeforeEach
    void setUp(){
        this.topo = new TopographicDetailsDTO();
    }

    @Test
    void shouldNotBeNull(){
        assertNotNull(this.topo);
    }

}
