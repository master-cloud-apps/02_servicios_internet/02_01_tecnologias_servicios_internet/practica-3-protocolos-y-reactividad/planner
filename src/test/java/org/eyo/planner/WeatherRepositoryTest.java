package org.eyo.planner;

import org.eyo.grpc.Weather;
import org.eyo.grpc.WeatherServiceGrpc;
import org.eyo.planner.repository.WeatherRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WeatherRepositoryTest {

    @InjectMocks
    private WeatherRepository weatherServiceRepository;

    @Mock
    private WeatherServiceGrpc.WeatherServiceBlockingStub client;

    @Test
    void whenGetMadridWeather_shouldReturnSunny() throws ExecutionException, InterruptedException {
        when(client.getWeather(any())).thenReturn(Weather.newBuilder().setWeather("Sunny").build());

        Weather madridWeather = weatherServiceRepository.getCityWeather("Madrid").get();

        assertEquals("Sunny", madridWeather.getWeather());
    }

    @Test
    void whenGetAlavaWeather_shouldReturnRainy() throws ExecutionException, InterruptedException {
        when(client.getWeather(any())).thenReturn(Weather.newBuilder().setWeather("Rainy").build());

        Weather madridWeather = weatherServiceRepository.getCityWeather("Alava").get();

        assertEquals("Rainy", madridWeather.getWeather());
    }
}
