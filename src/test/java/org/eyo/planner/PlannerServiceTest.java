package org.eyo.planner;

import org.eyo.grpc.Weather;
import org.eyo.planner.model.Plan;
import org.eyo.planner.model.PlanRequest;
import org.eyo.planner.model.TopographicDetailsDTO;
import org.eyo.planner.repository.TopoService;
import org.eyo.planner.repository.WeatherRepository;
import org.eyo.planner.service.PlannerServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class PlannerServiceTest {

    @InjectMocks
    private PlannerServiceImpl plannerService;
    @Mock
    private TopoService topoService;
    @Mock
    private WeatherRepository weatherRepository;
    @Mock
    private RabbitTemplate rabbitTemplate;

    @Test
    void whenGetPlanForMadrid_shouldReturnResponseFromTopoAndWeatherInLowerCase() throws ExecutionException,
            InterruptedException {
        when(this.topoService.getTopoFromCityName(anyString())).thenReturn(CompletableFuture.completedFuture(new TopographicDetailsDTO("Madrid", "flat")));
        when(this.weatherRepository.getCityWeather(anyString())).thenReturn(CompletableFuture.completedFuture(Weather.newBuilder().setWeather("Sunny").build()));

        Plan madridPlan = this.plannerService.getPlanFromCity(new PlanRequest(0, "Madrid")).get();

        assertEquals("madrid-flat-sunny", madridPlan.getPlanning());
    }

    @Test
    void whenGetPlanForPontevedra_shouldReturnResponseFromTopoAndWeatherInUpperCase() throws ExecutionException,
            InterruptedException {
        when(this.topoService.getTopoFromCityName(anyString())).thenReturn(CompletableFuture.completedFuture(new TopographicDetailsDTO("Pontevedra", "mountain")));
        when(this.weatherRepository.getCityWeather(anyString())).thenReturn(CompletableFuture.completedFuture(Weather.newBuilder().setWeather("Rainy").build()));

        Plan madridPlan = this.plannerService.getPlanFromCity(new PlanRequest(0, "Pontevedra")).get();

        assertEquals("PONTEVEDRA-MOUNTAIN-RAINY", madridPlan.getPlanning());
    }
}
